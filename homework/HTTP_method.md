# HTTP 메소드 종류

## GET ★★★★★
- Server가 HTTP 요청 라인에 포함된 URL이 지정하는 자원을 찾아 Client에게 전송
- 조건부(Conditional) GET : Server에게 특정 조건이 만족할 경우 자원을 보낼 것을 요청. "If-Modified-Since" 또는 "If-Match"와 같은 헤더를 사용할 경우
- 부분적(Partial) GET : 보통 큰 파일을 요청할 때 쓰이는 것으로 자원의 일부분만 보내주기를 요청하는 Range 헤더를 사용할 경우
- 가장 많이 사용
- 웹 브라우저로 웹 사이트에 접속해 페이지를 다운로드하는 경우에 사용한다.
- 일반적인 HTML이나 자바 스크립트와 같은 클라이언트 측 언어는 직접 다운로드 받아 브라우저에서 실행하고 해석하며, CGI나 PHP, ASP와 같은 서버 측 언어의 경우에는 서버에서 실행된 결과를 다운로드 받아 실행한다.


## POST ★★★★★
- Client가 Server에 데이터를 보낼 때 사용
- 주로 게시판이나 자료실 등에 글이나 이미지 등을 업로드할 때 사용되는 메소드다.
- POST 요청 방식은 요청 URI(URL)에 폼 입력을 처리하기 위해 구성한 서버 측 스크립트(ASP, PHP, JSP 등) 혹은 CGI 프로그램으로 구성되고 Form Action과 함께 전송되는데, 이때 헤더 정보에 포함되지 않고 데이터 부분에 요청 정보가 들어가게 된다.
- 서버가 처리할 수 있는 자료를 보낸다. GET으로 보낼 수 없는 자료들에 대해 전송할 때 사용한다.

## HEAD ★★
- 웹 서버에서 수신되는 응답코드를 통해 페이지의 존재 여부를 알 수 있다.
- 실제 데이터는 다운로드하지 않고 파일과 웹 서버에 대한 정보만을 요청한다.
- 주로 검색 엔진에서 해당 파일의 존재유무를 체크
- 모니터링 프로그램에서 웹 사이트의 다운 유무 체크
- 웹 서버 버전 스캔

## OPTIONS
- 해당 메소드를 통해 시스템에서 지원되는 메소드 종류를 확인할 수 있다.

## PUT
- POST와 유사한 전송 구조를 가지기 때문에 헤더 이외에 메시지(데이터)가 함께 전송된다.
- 원격지 서버에 지정한 콘텐츠를 저장하기 위해 사용되며 홈페이지 변조에 많이 악용되고 있다.
- GET이 서버의 데이터를 브라우징하는 것이라면, POST는 반대로 클라이언트가 서버에 데이터를 보낼 때 사용
- 주로 게시판이나 자료실 등에 글이나 이미지 등을 업로드할 때 사용

## DELETE
- 원격지 웹 서버에 파일을 삭제하기 위해 사용되며 PUT과는 반대 개념의 메소드이다.
- 해당 URL의 자원, 정보를 삭제한다.

## TRACE
- 원격지 서버에 Loopback(루프백) 메시지를 호출하기 위해 사용된다.
- 이전까지 요청한 정보들의 목록을 요청한다.

## CONNECT
- 웹 서버에 프록시 기능을 요청할 때 사용된다.
