#JavaScript Hoisting

## 설명
[참고 사이트]: http://www.w3schools.com/js/js_hoisting.asp
- 현재 범위(현재 스크립트의 상단 또는 현재 함수)의 상단에 모든 선언을 이동하는 자바 스크립트의 기본 동작.
- If a developer doesn't understand hoisting, programs may contain bugs (errors)  
  : 만약 개발자가 hoisting을 이해하지 못하면, 프로그램은 버그(오류)를 포함 할 수 있다.
- To avoid bugs, always declare all variables at the beginning of every scope  
  : 버그를 피하기 위해, 항상 모든 범위의 처음에 모든 변수를 선언한다.

-------
다음은 hoisting 현상의 예 이다.  

	<script>
		var a;

		if(true){
			b = 1;
		}

		console.log("b = " + b);

		var b;
	</script>

변수 a는 선언만 하고 초기화 되지 않았다.  
변수 b는 if문 안에 초기화는 되어있으나 선언은 consolr.log 보다도 나중에 하였다.  
이 경우 b의 결과는 어떻게 나올까?
    
다음은 결과화면이다.

	b = 1;

변수 선언이 한참 나중에 되었더라도 위 결과와 같이 정상적이게 b = 1 이라는 값을 보여준다.  
그 이유가 바로 아래 보이는 코드와 같이 자바스크립트가 해석하였기 때문이며, 이 현상이 바로 hoisting이다.

	<script>
		var a;
		var b;

		if(true){
			b = 1;
		}

		console.log(b);
	</script>


## 단어장
> 1) Hoisted : 게양하다?  
  1.1) hoist : 들어올리다.  
     
> 2) Initializations : 초기화  
> 3) avoid : 기피, 무효, 피하다  
> 4) declare : 선언하다